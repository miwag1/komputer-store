# Komputer Store



## Assigment 1 -Create a Dynamic Webpage using JS-

Build a dynamic webpage using “vanilla” JavaScript (I use BootStrap´s CSS via CDN)

## Deacription

This app has 4 sections to show the functions to earn your salary, trasfer earned money to a bank, get a loan, repay loan, select a laptop, show the information of it and buy it.   

```
1) The Bank
2) Work
3) Laptops
4) Info of the selected computer
```

## The Bank

- Get a loan-Button: When the Get a loan button is clicked, it shows a prompt modal that allows you to enter an amount 
***
 You cannot get a loan 
 - more than double of your bank balance
 - more than one bank loan before repaying the last loan
***
- Repay loan-Button: This button appears when you hava a loan and when your loan becomes ZERO it disappears  (Reset your pay/salary once you transfer)

## Work

- Bank-Button: If you have a loan, 10% of your pay must be transferred to reduce loan amount and the rest of your money to your bank account (Reset your pay/salary once you transfer)
- Work-Button: Increase your Pay balance at a rate of 100 on each click

## Laptops

- Computer-Select box:Use API and show each title of laptops and show description of the selected laptop

## Info of the selected computer

- Buy now-Button: You can buy if you hava enough money in your bank account and the amount must be deducted from your account (A message must be shown that you cannot buy a laptop or you are the owner of the new laptop)
- Use API and show image, title, specs and price of the selected laptop
