const computersElement = document.getElementById("computers");
const featuresElement = document.getElementById("features");
const c_titleElement = document.getElementById("c_title");
const specsElement = document.getElementById("specs");
const priceElement = document.getElementById("price");
const imageElement = document.getElementById("img");
const c_imgElement = document.getElementById("c_img");


const addLoanElement = document.getElementById("add_loan");
const inputLoanElement = document.getElementById("input_loan");
const balanceElement =document.getElementById("balance");
const loanElement = document.getElementById("loan");

const repayLoanButtonElement = document.getElementById("repay_loan");

const workButtonElement = document.getElementById("work");
const payElement= document.getElementById("pay");

const bankButtonElement = document.getElementById("bank");

const buyButtonElement = document.getElementById("buy");


let computers =[];

let balance =200; //default balance
let totalLoan=0;

let pay=0; //default balance

const api_url= "https://hickory-quilled-actress.glitch.me/computers";
const image_url = "https://hickory-quilled-actress.glitch.me/";

fetch(api_url)
    .then(response =>response.json())
    .then(data => computers =data)
    .then(computers => addComputersToMenu(computers) )
   

const addComputersToMenu = (computers) =>{
    computers.forEach(e => addComputerToMenu(e));
    featuresElement.innerText=computers[0].description;// default features/API:description
    c_titleElement.innerText=computers[0].title;// default c_title/API:title
    specsElement.innerText=computers[0].specs;// default specs
    priceElement.innerText=computers[0].price;// default price 
    imageElement.src=image_url +computers[0].image;// default image
    c_imgElement.href=image_url +computers[0].image; // default image for click
}

const addComputerToMenu=(computer) =>{
    const computerElement = document.createElement("option")
    computerElement.value=computer.id;
    computerElement.appendChild(document.createTextNode(computer.title));
    computersElement.appendChild(computerElement);
}

const handleComputerMenuChange = e=>{
    const selectedComputer =computers[e.target.selectedIndex];
    featuresElement.innerText=selectedComputer.description;
    c_titleElement.innerText=selectedComputer.title;
    specsElement.innerText=selectedComputer.specs;
    priceElement.innerText=selectedComputer.price;
    imageElement.src = image_url + selectedComputer.image
    c_imgElement.href = image_url + selectedComputer.image
}

computersElement.addEventListener("change", handleComputerMenuChange);


// ---Add Loan------------

/*
Submit your loan-> increce your balance and loan
When you get a lone Repay loan-Button appears
Show the message if you tryed to get a loan that do not meet the requirements
*/

repayLoanButtonElement.style.visibility="hidden"; // Default Do not desplay Repay loan-Button

const handleAddLoan = () =>{

    const loan = parseInt(inputLoanElement.value);
    

  if(loan>balance*2){
    alert("You cannot get a loan more than double of your bank balance");
  } else if(totalLoan>0){
    alert("You cannot get more than one bank loan before repaying the last loan");
  } else{

    totalLoan+=loan;
    repayLoanButtonElement.style.visibility="visible";
    balance += loan;
    balanceElement.innerText=`${balance}`;
    loanElement.innerText=`${totalLoan}`
}

}

addLoanElement.addEventListener("click", handleAddLoan);

function clearText(){  
    document.getElementById("input_loan").value=0;  
  } 



//-----Add Repay Loan Button--------------  

/*
IT DOES NOT WORK
if you try to submit from prompt 
When you get a lone which ore than double of your bank balance the button appears...
I used Onclick ="displayButton()" to submit button id: add_loan
When you reduce your loan to Zero the button disappears ->
*/ 
/*
 repayLoanButtonElement.style.visibility="hidden"; // Default Do not desplay Repay loan button

  function displayButton(){
    const loan = parseInt(inputLoanElement.value);
    
    if(loan>0)
    repayLoanButtonElement.style.visibility="visible";
    console.log("LOAN"+loan)
    console.log("Total loan"+totalLoan)
    
 }

 */

 

// ---Work Button------------

 /*
Increce 100Kr when you click the button
*/


/*  workButtonElement.addEventListener("click",()=>{

    pay+=100;
    payElement.innerText=`${pay}`
  //payElement.innerText+=parseInt(100)
 }) */



 const handleWorkButton = () =>{
    pay+=100;
    payElement.innerText=`${pay}`;
 }

 workButtonElement.addEventListener("click", handleWorkButton);



// ---Bank Button------------
/*
1.10% of pay balance should be transferrred to reduce loan
2.90% of pay balance should be transfeerred to balance
*/

const handleBankButton =() =>{
    const transferredMoneyToReduceLoan=pay*0.1;
    const transferredMoneyToBalance=pay*0.9;

    if(totalLoan>transferredMoneyToReduceLoan){
        totalLoan=totalLoan-transferredMoneyToReduceLoan;
        balance +=transferredMoneyToBalance;
        balanceElement.innerText=`${balance}`;
        loanElement.innerText=`${totalLoan}`;

    }else{
        balance +=transferredMoneyToBalance+(transferredMoneyToReduceLoan-totalLoan);
        totalLoan=0;
        repayLoanButtonElement.style.visibility="hidden";
        balanceElement.innerText=`${balance}`;
        loanElement.innerText=`${totalLoan}`;
    
}

    pay=0;
    payElement.innerText=`${pay}`;
    

}

bankButtonElement.addEventListener("click", handleBankButton)



// ---Repay loan Button------------

/*
1. 100% of pay balance should be transferred to reduce loan 
2. The rest of pay balance should be transferred to balance
*/

const handleRepayLoanButton =() =>{
    const transferredFullMoney=pay;

    if(totalLoan>pay){
        totalLoan=totalLoan-transferredFullMoney;
        loanElement.innerText=`${totalLoan}`;

    }else{
        balance +=transferredFullMoney-totalLoan;
        totalLoan=0;
        repayLoanButtonElement.style.visibility="hidden";
        balanceElement.innerText=`${balance}`;
        loanElement.innerText=`${totalLoan}`;
    
}

    pay=0;
    payElement.innerText=`${pay}`;
    

}

 repayLoanButtonElement.addEventListener("click", handleRepayLoanButton)


// ---Buy Button------------
/*
You can buy a laptop if you have enopugh money (Get a message) and reduce money from your balance
you get a message if you do not have enough money  

*/


const handleBuyButton =() => {
    const selectedComputer = computers[computersElement.selectedIndex];
    const selectedC_Price= selectedComputer.price;
    if(balance>=selectedC_Price){
        balance= balance-selectedC_Price;
        balanceElement.innerText=`${balance}`;
        alert("You are now the owner of the new laptop!");
    } else {
        alert("You cannot afford the laptop because you do not have enough money in your account.");
    }

}

buyButtonElement.addEventListener("click", handleBuyButton)